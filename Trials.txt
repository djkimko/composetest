gcloud container clusters create syntro-cluster --num-nodes=2 --region us-central1-a

gcloud container clusters list
gcloud compute instances list

curl -L https://github.com/kubernetes/kompose/releases/download/v1.16.0/kompose-linux-amd64 -o kompose



VM way (docker only)
-- (NOT working, permission issues)https://cloud.google.com/community/tutorials/docker-compose-on-container-optimized-os
https://djkimko@bitbucket.org/djkimko/composetest.git


# This script is meant for quick & easy install via:
#   $ curl -fsSL https://get.docker.com -o get-docker.sh
#   $ sh get-docker.sh
 sudo usermod -aG docker djkimko

#docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

#git
sudo apt update
sudo apt install git

#Open 7474 and 7687
https://docs.bitnami.com/google/faq/administration/use-firewall/